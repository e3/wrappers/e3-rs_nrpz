#
#  Copyright (c) 2019-2021    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Douglas Bezerra Beniz
# email   : douglas.bezerra.beniz@ess.eu
# Date    : 2020okt06-1822-24CEST
# version : 0.0.0 
#
# template file is generated by ./e3TemplateGenerator.bash with f774945
# Please look at many other _module_.Makefile in e3-* repository
# 

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



# If one would like to use the module dependency restrictly,
# one should look at other modules makefile to add more
# In most case, one should ignore the following lines:

## Exclude linux-ppc64e6500
EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

APP:=rs_nrpzApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

# -----------------------------------------------------------------------------
# Rohde & Schwarz vendor libraries settings
# -----------------------------------------------------------------------------
SUPPORT:=vendor
VENDOR_PKG_DIR:=usr

# -----------------------------------------------------------------------------
# Rohde & Schwarz libraries (prebuild) used during COMPILATION time
# -----------------------------------------------------------------------------
# Use -Wall -g3 -O0 -DDEBUG for debug build
#USR_INCLUDES += -I/usr/include/
USR_INCLUDES += -I../$(VENDOR_PKG_DIR)/include

USR_LDFLAGS += -Wl,--enable-new-dtags
USR_LDFLAGS += -Wl,-rpath=$(E3_MODULES_VENDOR_LIBS_LOCATION)
USR_LDFLAGS += -L$(E3_MODULES_VENDOR_LIBS_LOCATION)
USR_LDFLAGS += -L$(VENDOR_PKG_DIR)/lib64

# -----------------------------------------------------------------------------
# Shared objects
# -----------------------------------------------------------------------------
VENDOR_TAR  += $(SUPPORT)/NrpToolkit_CentOS_7_x86_64.tar.gz

VENDOR_PKGS += $(SUPPORT)/x86_64/libnrp-4.21-1.el7.x86_64.rpm
VENDOR_PKGS += $(SUPPORT)/x86_64/libnrp-devel-4.21-1.el7.x86_64.rpm
VENDOR_PKGS += $(SUPPORT)/x86_64/librsnrpz-3.7.0-1.el7.x86_64.rpm
VENDOR_PKGS += $(SUPPORT)/x86_64/librsnrpz-devel-3.7.0-1.el7.x86_64.rpm

VENDOR_LIBS += $(VENDOR_PKG_DIR)/lib64/libnrp.so.0.0.0
VENDOR_LIBS += $(VENDOR_PKG_DIR)/lib64/libnrp.so.0
VENDOR_LIBS += $(VENDOR_PKG_DIR)/lib64/libnrp.so
VENDOR_LIBS += $(VENDOR_PKG_DIR)/lib64/librsnrpz.so.0.0.0
VENDOR_LIBS += $(VENDOR_PKG_DIR)/lib64/librsnrpz.so.0
VENDOR_LIBS += $(VENDOR_PKG_DIR)/lib64/librsnrpz.so


# -----------------------------------------------------------------------------
# Rohde & Schwarz libraries (prebuild) used during EXECUTION time
# -----------------------------------------------------------------------------
#USR_LDFLAGS  += -lrt -lnrp -lrsnrpz
#USR_LDFLAGS  += -L../$(VENDOR_PKG_DIR)/lib64

LIB_SYS_LIBS += rsnrpz
LIB_SYS_LIBS += nrp


# -----------------------------------------------------------------------------
#  # IOC library name when installed
# -----------------------------------------------------------------------------
LIBRARY_IOC += rs_nrpz

# -----------------------------------------------------------------------------
# HEADERS
# -----------------------------------------------------------------------------
HEADERS += $(APPSRC)/rs_nrpz.h
# -----------------------------------------------------------------------------
# C/C++ SOURCES
# -----------------------------------------------------------------------------
SOURCES += $(APPSRC)/rs_nrpz.cpp

# -----------------------------------------------------------------------------
# EPICS database definitions
# -----------------------------------------------------------------------------
DBDS    += $(APPSRC)/rs_nrpzSupport.dbd

# -----------------------------------------------------------------------------
# EPICS shell scripts
# -----------------------------------------------------------------------------
SCRIPTS += $(wildcard ../iocsh/*.iocsh)


# -----------------------------------------------------------------------------
# EPICS database files to be deployed
# -----------------------------------------------------------------------------
#TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/rs_nrpz_sensors_hub.db)
TEMPLATES += $(wildcard $(APPDB)/rs_nrpz_sensor.db)


# -----------------------------------------------------------------------------
# EPICS database compilation
# -----------------------------------------------------------------------------
USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

SUBS=$(wildcard $(APPDB)/*.substitutions)
TMPS=$(wildcard $(APPDB)/*.template)


vlibs:

.PHONY: vlibs


# -----------------------------------------------------------------------------
# Vendor libraries extraction
# -----------------------------------------------------------------------------
.PHONY: $(VENDOR_TAR)
$(VENDOR_TAR):
	$(QUIET) tar -C $(SUPPORT) -xzvf $@

.PHONY: $(VENDOR_PKGS)
$(VENDOR_PKGS): $(VENDOR_TAR)
	$(QUIET) rpm2cpio $@ | cpio --extract --make-directories

.PHONY: $(VENDOR_LIBS)
$(VENDOR_LIBS): $(VENDOR_PKGS)
	$(QUIET) patchelf --set-rpath "\$$ORIGIN" $@

.PHONY: prebuild
prebuild: $(VENDOR_LIBS)
